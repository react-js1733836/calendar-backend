const {response} = require('express');
const bcrypt = require('bcryptjs');
const Usuario = require('../models/Usuario');
const { generarJWT } = require('../helpers/jwt');

const crearUsuario =async (req, res = response) => {
    
    const {email, password} = req.body;
    
    try {
        //* Validar si existe ya un email creado
        let usuario = await Usuario.findOne({email: email});
        if(usuario){
            return res.status(400).json({
                ok: false,
                msg: 'Un Usuario existe con ese correo.'
            });
        }
        usuario = new Usuario(req.body);
        //* Encriptar contraseña
        const salt = bcrypt.genSaltSync();
        usuario.password = bcrypt.hashSync(password, salt);
        //* Guardar en la base de datos
        await usuario.save();
        //* Generar JWT
        const token =await generarJWT(usuario._id, usuario.name);
        //* Respuesta
        res.status(201).json({
            ok: true,
            uid: usuario._id,
            name: usuario.name,
            token
        });

    } catch (error) {
        console.log(error);
        res.status(500).json({
            ok: false,
            msg: 'Por favor, hable con el administrador.'
        });
    }
}

const loginUsuario =async (req, res = response) => {
    const {email, password} = req.body;
    try {
        //* Validar si existe ya un email creado
        const usuario = await Usuario.findOne({email: email});
        //* Si retornar algo es por que si existe entonces se realiza la negación
        if(!usuario){
            return res.status(400).json({
                ok: false,
                msg: 'El usuario no existe con ese email.'
            });
        }
        //* Confirmar las contraseñas
        const validPassword = bcrypt.compareSync(password, usuario.password);
        if(!validPassword){
            res.status(400).json({
                ok:false,
                msg: 'Password incorrecto.'
            })
        }
        //* Generar JWT
        const token =await generarJWT(usuario._id, usuario.name);
        //* Respuesta 
        res.json({
            ok: true,
            uid: usuario._id,
            name: usuario.name,
            token
        });
    } catch (error) {
        console.log(error);
        res.status(500).json({
            ok: false,
            msg: 'Por favor, hable con el administrador'
        });
    }

}

const revalidarToken =async (req, res = response) => {
    //* Obtener de la req el uid y el name del middleware
    const {uid, name} = req;
    //* Generar un nuevo JWT
    const token = await generarJWT(uid, name);
    //* Respuesta
    res.json({
        ok: true,
        uid,
        name,
        token
    });
}

module.exports = {
    crearUsuario,
    loginUsuario,
    revalidarToken
}