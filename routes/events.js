/*
  Rutas de eventos / 
  host + /api/events/
*/
const { Router } = require('express');
const { check } = require('express-validator');
const { getEventos, crearEvento, actualizarEvento, borrarEvento } = require('../controllers/events');
const { validarJWT } = require('../middlewares/validar-jwt');
const { validarCampos } = require('../middlewares/validar-campos');
const isDate = require('../helpers/isDate');
const router = Router();

//* Validar todas las rutas
router.use(validarJWT);

//* Obtener Eventos
router.get('/', getEventos);

//* Crear Evento
router.post('/', [
    check('title', 'El titulo es obligatorio.').not().isEmpty(),
    check('start', 'La fecha inicial es obligatoria.').custom(isDate),
    check('end', 'La fecha final es obligatoria.').custom(isDate),
    // TODO: Middleware para validar los campos
    validarCampos
], crearEvento);

//* Actualizar Evento
router.put('/:id', actualizarEvento);

//* Borrar Evento
router.delete('/:id', borrarEvento);


module.exports = router;

