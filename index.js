const express = require('express');
const { dbConnection } = require('./database/config');
const cors = require('cors');
require('dotenv').config();

//* crear el servidor de express
const app = express();

//* Base de Datos
dbConnection();

//* CORS   
app.use(cors());

//* directorio publico
app.use(express.static('public'));

//* Lectura y parseo del body
app.use(express.json());

//* Rutas
// TODO: auth // crear, Login, Renew
app.use('/api/auth', require('./routes/auth'));
// TODO: CRUD: Eventos
app.use('/api/events', require('./routes/events'));


//* Escuchar las peticiones
app.listen(process.env.PORT, () => {
    console.log(`Servidor corriend en puerto ${process.env.PORT}`);
});