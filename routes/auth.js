/*
  Rutas de Usuarios / Auth
  host + /api/auth
*/
const { Router } =require('express');
const { check } = require('express-validator');
const router = Router();
const { crearUsuario, loginUsuario, revalidarToken } = require('../controllers/auth');
const { validarCampos } = require('../middlewares/validar-campos');
const { validarJWT } = require('../middlewares/validar-jwt');

// * Crear nuevo usuario
router.post(
  '/new', 
  [ //* Middlewares
    check('name', 'El nombre es obligatorio.').not().isEmpty(),
    check('email', 'El email es obligatorio.').isEmail(),
    check('password', 'El password debe de ser de 6 caracteres.').isLength({min: 6}),
    // TODO: Middleware para validar los campos
    validarCampos
  ], 
  crearUsuario);

// * Login de usuario
router.post(
  '/',
  [ // * Middlewares
    check('email', 'El email es obligatorio.').isEmail(),
    check('password', 'El password debe de ser de 6 caracteres.').isLength({min: 6}),
    // TODO: Middleware para validar los campos
    validarCampos
  ],
  loginUsuario);

// * Renovar Token
router.get('/renew', 
  // * Middlewar para validar el token
  validarJWT,
  revalidarToken);

//* Exportar el router
module.exports = router;
